package com.iggirex.simpledotcomsgame.tests;

import com.iggirex.dotcomsgame.BoardCell;
import com.iggirex.dotcomsgame.DotCom;
import com.iggirex.dotcomsgame.Helper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class HelperTest {

    Helper helperClass;

    @Before()
    public void setupTests() {
        helperClass = new Helper();
        helperClass.initiateGrid();
        helperClass.createDotComs();
    }

    @Test
    public void initiateGridTest() {
        ArrayList<ArrayList<BoardCell>> testGrid = helperClass.getGrid();

        Assert.assertEquals(7, testGrid.size());
        Assert.assertEquals("A0", testGrid.get(0).get(0).getValue());
        Assert.assertEquals("A6", testGrid.get(0).get(testGrid.get(0).size() - 1).getValue());
        Assert.assertEquals("G0", testGrid.get(testGrid.get(0).size() - 1).get(0).getValue());
        Assert.assertEquals("G6", testGrid.get(testGrid.get(0).size() - 1).get(testGrid.get(0).size() - 1).getValue());
    }

    @Test
    public void createDotComsTest() {
        Assert.assertEquals(3, helperClass.shipsList.size());
        Assert.assertEquals("Pets.com", helperClass.shipsList.get(0).getName());
        Assert.assertEquals(4, helperClass.shipsList.get(2).getCoordinates().size());
    }

    @Test
    public void makeNewShipCoordinatesTest() {
        ArrayList<int[]> upDownCoordinates = helperClass.makeNewShipCoordinates("upDown");
        Assert.assertEquals(4, upDownCoordinates.size());

        for (int i=0; i<upDownCoordinates.size(); i++) {
            Assert.assertTrue("New X Coordinate " + upDownCoordinates.get(i)[0] + " is >= 0", upDownCoordinates.get(i)[0] >= 0);
            Assert.assertTrue("New X Coordinate " + upDownCoordinates.get(i)[0] + " is < 7", upDownCoordinates.get(i)[0] < 7);

            Assert.assertTrue("New Y Coordinate " + upDownCoordinates.get(i)[1] + " is >= 0", upDownCoordinates.get(i)[1] >= 0);
            Assert.assertTrue("New Y Coordinate " + upDownCoordinates.get(i)[1] + " is < 7", upDownCoordinates.get(i)[1] < 7);
        }

        ArrayList<int[]> sideSideCoordinates = helperClass.makeNewShipCoordinates("sideToSide");
        Assert.assertEquals(4, sideSideCoordinates.size());

        for (int i=0; i<sideSideCoordinates.size(); i++) {
            Assert.assertTrue("New X Coordinate is >= 0", sideSideCoordinates.get(i)[0] >= 0);
            Assert.assertTrue("New X Coordinate is < 4", sideSideCoordinates.get(i)[0] < 7);

            Assert.assertTrue("New Y Coordinate is >= 0", sideSideCoordinates.get(i)[1] >= 0);
            Assert.assertTrue("New Y Coordinate is < 7 0", sideSideCoordinates.get(i)[1] < 7);
        }
    }

    @Test
    public void placeAllShipsTest() {
        helperClass.placeAllShips();
        int[] controlCoordinates = new int[2];

        ArrayList<DotCom> shipList = helperClass.getShipsList();

        // compare each coordinate to each other coordinate for all ships to make sure all are unique
        for(int i=0; i<shipList.size(); i++) {

            DotCom controlShip = shipList.get(i);

            for(int x=0; x<controlShip.getCoordinates().size(); x++){

                controlCoordinates = controlShip.getCoordinates().get(x);

                for(int w=0; w<shipList.size(); w++) {

                    DotCom comparisonShip = shipList.get(w);

                    for (int z=0; z<comparisonShip.getCoordinates().size(); z++) {

                        int[] comparisonCoordinates = comparisonShip.getCoordinates().get(z);

                        // if controlShip is not in same iteration as comparisonShip
                        // and if controlShip's coordinates are not in same iteration as comparisonShip's coordinates
                        // go ahead and compare
                        // TLDR; Do not compare the control coordinate against itself (when inner loop iterates over it)
                        if (i != w || x != z) {
                            Assert.assertTrue(x + " coordinate of " + controlShip.getName() + " is unique",
                            !Arrays.equals(controlCoordinates, comparisonCoordinates));
                        }
                    }
                }
            }
        }

        // make sure all coordinates are within the board limits
        for (DotCom ship : shipList) {
            for(int a=0; a<ship.getCoordinates().size(); a++) {
                Assert.assertTrue("X Coordinate is not less than zero", ship.getCoordinates().get(a)[0] >= 0);
                Assert.assertTrue("X Coordinate is not more than six", ship.getCoordinates().get(a)[0] < 7);

                Assert.assertTrue("Y Coordinate is not less than zero", ship.getCoordinates().get(a)[1] >= 0);
                Assert.assertTrue("Y Coordinate is not more than six", ship.getCoordinates().get(a)[1] < 7);
            }
        }
    }

    @Test
    public void checkForAHitTest() {
        int[] correctUserGuess = new int[]{0,0};
        int[] incorrectUserGuess = new int[]{6,6};
        helperClass.shipsList.get(0).getCoordinates().remove(0);
        helperClass.shipsList.get(0).getCoordinates().add(0, correctUserGuess);

        Assert.assertEquals(true, helperClass.checkForAHit(correctUserGuess));

        helperClass.shipsList.get(2).getCoordinates().remove(3);
        helperClass.shipsList.get(2).getCoordinates().add(3, new int[]{1,2});

        Assert.assertEquals(false, helperClass.checkForAHit(incorrectUserGuess));
    }
}
