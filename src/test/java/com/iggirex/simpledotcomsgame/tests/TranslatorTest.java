package com.iggirex.simpledotcomsgame.tests;

import com.iggirex.dotcomsgame.Translator;
import org.junit.Assert;
import org.junit.Test;

public class TranslatorTest {

    Translator translator = new Translator();

    @Test
    public void testTranslate(){
        try {
            String testCoordinates = "A0";
            int [] translatedCoordinates = translator.translate(testCoordinates);
            int [] correctCoordinates = new int[]{0, 0};
            Assert.assertArrayEquals(correctCoordinates, translatedCoordinates);

            String testCoordinates2 = "C4";
            int [] translatedCoordinates2 = translator.translate(testCoordinates2);
            int [] correctCoordinates2 = new int[]{2, 4};
            Assert.assertArrayEquals(correctCoordinates2, translatedCoordinates2);

            String testCoordinates3 = "E6";
            int [] translatedCoordinates3 = translator.translate(testCoordinates3);
            int [] correctCoordinates3 = new int[]{4, 6};
            Assert.assertArrayEquals(correctCoordinates3, translatedCoordinates3);

        } catch (Error e) {
            throw e;
        }

    }
}
