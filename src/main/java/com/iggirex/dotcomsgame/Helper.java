package com.iggirex.dotcomsgame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import static com.iggirex.dotcomsgame.UserInputGetter.RED;

public class Helper {

    public int turnNumber = 0;
    public int numberOfShips = 3;
    public ArrayList<DotCom> shipsList = new ArrayList();
    public ArrayList<ArrayList<BoardCell>> grid = new ArrayList();
    char[] gridLetter = new char[7];
    ArrayList<String> shipNames = new ArrayList(Arrays.asList("Pets.com", "Ask Jeeves", "Napster"));
    String[] directions = new String[]{"upDown", "sideToSide"};
    ArrayList<int[]> missList = new ArrayList();
    ArrayList<int[]> hitList = new ArrayList();

    public static final String YELLOW = "\033[0;33m";
    public static final String GREEN = "\033[0;32m";
    public static final String WHITE = "\033[0;37m";

    public void initiateGrid() {
        gridLetter[0] = 'A';
        gridLetter[1] = 'B';
        gridLetter[2] = 'C';
        gridLetter[3] = 'D';
        gridLetter[4] = 'E';
        gridLetter[5] = 'F';
        gridLetter[6] = 'G';

        for(int x = 0; x < 7; x++) {
            ArrayList<BoardCell> currentColumn = new ArrayList();
            grid.add(currentColumn);

            for(int y = 0; y < 7; y++) {
                String currentRow = gridLetter[x] + String.valueOf(y);

                BoardCell currentBoardCell = new BoardCell(currentRow);
                currentBoardCell.setValue(currentRow);
                currentBoardCell.setCoordinates(new int[] {x, y});
                currentColumn.add(currentBoardCell);
            }
        }
    }

    public void createDotComs() {
        Random rand = new Random();
        for(int i=0; i<numberOfShips; i++) {
            DotCom newShip = new DotCom(shipNames.get(i));
            newShip.setDirection(directions[rand.nextInt(2)]);
            newShip.setCoordinates(new ArrayList<int[]>());

            for (int x=0; x<4; x++) {
                ArrayList<int[]> moreCoordinates = new ArrayList<int[]>();
                moreCoordinates = newShip.getCoordinates();
                moreCoordinates.add(new int[]{-1, -1});
                newShip.setCoordinates(moreCoordinates);
            }
            shipsList.add(newShip);
        }
    }

    public ArrayList<int[]> makeNewShipCoordinates(String direction) {

        Random rand = new Random();
        int x = rand.nextInt(4);
        int y = rand.nextInt(4);
        ArrayList<int[]> newShipCoordinates = new ArrayList<int[]>();

        try {
            if (direction == "upDown") {
                int newX = rand.nextInt(7);

                newShipCoordinates.add(new int[]{newX, y});
                newShipCoordinates.add(new int[]{newX, y + 1});
                newShipCoordinates.add(new int[]{newX, y + 2});
                newShipCoordinates.add(new int[]{newX, y + 3});
            } else if (direction == "sideToSide") {
                int newY = rand.nextInt(7);

                newShipCoordinates.add(new int[]{x, newY});
                newShipCoordinates.add(new int[]{x + 1, newY});
                newShipCoordinates.add(new int[]{x + 2, newY});
                newShipCoordinates.add(new int[]{x + 3, newY});
            }
        } catch (Exception e) {
            System.out.println("Ship does not have a direction set yet\n" + e);
        }
        return newShipCoordinates;
    }


//        public void placeOneShip(DotCom ship) {
        public void placeAllShips() {

        recursiveLoop : {
            for (DotCom ship : shipsList) {
                // figure out all 4 coordinates this ship will be at
                ArrayList<int[]> newShipCoordinates = makeNewShipCoordinates(ship.getDirection());

                // keep track if this iteration's newShipCoordinates collide with other ships
                Boolean canPlaceShipInIteration = true;

                // go through each ship to checkIfShipsCollide(newCoordinates);
                for (int i = 0; i < newShipCoordinates.size(); i++) {
                    int[] newCoordinateToPlace = newShipCoordinates.get(i);

                    for (DotCom placedShip : shipsList) {
                        for (int x = 0; x < placedShip.getCoordinates().size(); x++) {

                            int[] comparisonCoordinates = placedShip.getCoordinates().get(x);

                            if (placedShip != ship || i != x) {
                                if (Arrays.equals(comparisonCoordinates, newCoordinateToPlace)) {
//                                    canPlaceShipInIteration = false;
                                    resetAllShipCoordinates();
                                    placeAllShips();
                                    break recursiveLoop;
                                }
                            }
                        }
                    }
                }
                // after each new coordinate gets checked against all coordinates, set this new coordinate
                if (canPlaceShipInIteration) {
                    ship.setCoordinates(newShipCoordinates);
                }
            }
        }
    }

    public void resetAllShipCoordinates() {
        for (DotCom ship : shipsList) {
            int[] resetArray = new int[] {-1, -1};
            ArrayList<int[]> resetCoordinates = new ArrayList<int[]>();
            resetCoordinates.add(resetArray);
            resetCoordinates.add(resetArray);
            resetCoordinates.add(resetArray);
            resetCoordinates.add(resetArray);
            ship.setCoordinates(resetCoordinates);
        }
    }

    public void runGame() {
        UserInputGetter userInputGetter = new UserInputGetter();
        int amountOfShipCoordinatesLeft = 12;

        TextVisualizer textVisualizer = new TextVisualizer();
        textVisualizer.showGameTitle();
        textVisualizer.showCredits();

        while(true) {
            turnNumber++;

            BoardVisualizer boardVisualizer = new BoardVisualizer();
            boardVisualizer.visualizeBoard(grid, hitList, missList);

            int[] userGuess = userInputGetter.getUserInput(turnNumber);

            if(userGuess == null) {
                continue;
            }

            Boolean userDidHit = checkForAHit(userGuess);
            if (userDidHit) {
                amountOfShipCoordinatesLeft--;
            }
            if (amountOfShipCoordinatesLeft == 0) {
                System.out.println(Config.CONGRATS + "YOU HAVE WON!!");
                break;
            }
        }
    }

    public Boolean checkForAHit(int[] userGuess) {
        for (int i=0; i<shipsList.size(); i++) {

            DotCom currentShip = shipsList.get(i);
            ArrayList<int[]> currentShipsCoordinatesList = currentShip.getCoordinates();

            for(int x=0; x<currentShipsCoordinatesList.size(); x++){

                int [] currentShipsCurrentCoordinatePair = currentShipsCoordinatesList.get(x);

                if (Arrays.equals(currentShipsCurrentCoordinatePair, userGuess)) {
                    hitList.add(userGuess);
                    currentShipsCoordinatesList.remove(x);

                    System.out.println(GREEN + Config.BOOM + " You have hit " + currentShip.getName() +
                            " on " + Arrays.toString(userGuess) + WHITE);

                    if (currentShipsCoordinatesList.size() == 0) {
                        System.out.println(RED + "You have sunk " + currentShip.getName() + "!!" + WHITE);
                    }
                    return true;
                }
            }
        }
        System.out.println(YELLOW + "MISS!! You have missed on " + Arrays.toString(userGuess) + WHITE);
        missList.add(userGuess);
        return false;
    }

    public ArrayList<ArrayList<BoardCell>> getGrid() {
        return grid;
    }

    public ArrayList<DotCom> getShipsList() {
        return shipsList;
    }

}
