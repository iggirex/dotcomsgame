package com.iggirex.dotcomsgame;

import java.util.ArrayList;
import java.util.Arrays;

import static com.iggirex.dotcomsgame.Helper.YELLOW;
import static com.iggirex.dotcomsgame.UserInputGetter.RED;
import static com.iggirex.dotcomsgame.UserInputGetter.WHITE;

public class BoardVisualizer {

    public void visualizeBoard(ArrayList<ArrayList<BoardCell>> grid, ArrayList<int[]> hitList, ArrayList<int[]> missList) {

        System.out.println("      |--------------------------------| \\");

        for ( ArrayList<BoardCell> numberRow : grid) {
            System.out.print("\n      |   ");

            for (int i=0; i<numberRow.size(); i++) {
                BoardCell currentBoardCell = numberRow.get(i);
                Boolean hasCellBeenTaken = false;

                int[] currentCoordinates = currentBoardCell.getCoordinates();

                for (int[] hit : hitList) {
                    if (Arrays.equals(hit, currentCoordinates)) {
                        System.out.print(RED + currentBoardCell.getValue() + "  " + WHITE);
                        hasCellBeenTaken = true;
                        break;
                    }
                }

                for (int[] miss : missList) {
                    if (Arrays.equals(miss, currentCoordinates) && !hasCellBeenTaken) {
                        System.out.print(YELLOW + currentBoardCell.getValue() + "  " + WHITE);
                        hasCellBeenTaken = true;
                        break;
                    }
                }

                if (!hasCellBeenTaken) {
                    System.out.print(currentBoardCell.getValue() + "  ");
                }
            }
            System.out.println(" | |");
        }

        System.out.println("\n      |________________________________| /");
    }
}
