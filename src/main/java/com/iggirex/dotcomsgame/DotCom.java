package com.iggirex.dotcomsgame;

import java.util.ArrayList;

public class DotCom {

    private String name;
    public ArrayList<int[]> coordinates;
    public String direction;

    public DotCom(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<int[]> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<int[]> coordinates) {
        this.coordinates = coordinates;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "DotCom{" +
                "name='" + name + '\'' +
                ", coordinates=" + coordinates +
                '}';
    }
}
