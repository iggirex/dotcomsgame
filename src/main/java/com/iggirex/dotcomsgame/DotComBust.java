package com.iggirex.dotcomsgame;

public class DotComBust {

    public static void main(String[] args) {
        Helper helper = new Helper();

        helper.initiateGrid();
        helper.createDotComs();
        helper.placeAllShips();
        helper.runGame();
    }
}
