package com.iggirex.dotcomsgame;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserInputGetter {

    public static final String RED = "\033[0;31m";
    public static final String WHITE = "\033[0;37m";

    public int[] getUserInput(int turnNumber) {
        Scanner myObj = new Scanner(System.in);
        Translator translator = new Translator();

        System.out.println("\nTurn #: " + turnNumber);
        System.out.println("Enter your guess, must be a letter from A to G followed by a number from 0 to 6");
        String userGuess = myObj.nextLine();

        if (userGuess.length() != 2) {
            try {
                throw new Exception (RED + "You did not enter two coordinates" + WHITE);
            } catch(Exception e) {
                System.out.println(e);
            }
            return null;
        }

        Pattern x = Pattern.compile("^[A-G]");
        Pattern y = Pattern.compile("[0-6]$");
        Matcher m1 = x.matcher(Character.toString(userGuess.charAt(0)));
        Matcher m2 = y.matcher(Character.toString(userGuess.charAt(1)));

        boolean b1 = m1.matches();
        boolean b2 = m2.matches();

        if (userGuess.length() <= 0) {
            try {
                throw new Exception (RED + "You did not enter two coordinates" + WHITE);
            } catch(Exception e) {
                System.out.println(e);
            }
            return null;
        }

        if (!b1) {
            try {
                throw new Exception (RED + "First coordinate was not a capital letter from A-G" + WHITE);
            } catch(Exception e) {
                System.out.println(e);
            }
            return null;
        }

        if (!b2) {
            try {
                throw new Exception (RED + "Second coordinate was not a number from 0-6" + WHITE);
            } catch(Exception e) {
                System.out.println(e);
            }
            return null;
        }

        return translator.translate(userGuess);
    };
}
