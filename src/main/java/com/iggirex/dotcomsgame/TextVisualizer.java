package com.iggirex.dotcomsgame;

public class TextVisualizer {

    Config config = new Config();

    final String title = config.GAME_TITLE;
    final String credits = config.GAME_CREDITS;

    public void showGameTitle() {
        System.out.println(title);
    }

    public void showCredits() {
        System.out.println(credits);
    }
}
