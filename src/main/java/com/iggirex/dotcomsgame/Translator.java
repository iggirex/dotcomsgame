package com.iggirex.dotcomsgame;

public class Translator {

    public int[] translate(String coordinates) {

        // takes a char and number string and
        // outputs location on grid

        char coordinateLetter = coordinates.charAt(0);
        int translatedCoordinateLetter = -1;
        int coordinateNumber = Character.getNumericValue(coordinates.charAt(1));

        int[] gridCoordinates = new int[2];

        switch(coordinateLetter) {
            case 'A': translatedCoordinateLetter = 0;
                break;
            case 'B': translatedCoordinateLetter = 1;
                break;
            case 'C': translatedCoordinateLetter = 2;
                break;
            case 'D': translatedCoordinateLetter = 3;
                break;
            case 'E': translatedCoordinateLetter = 4;
                break;
            case 'F': translatedCoordinateLetter = 5;
                break;
            case 'G': translatedCoordinateLetter = 6;
                break;
        }

        gridCoordinates[0] = translatedCoordinateLetter;
        gridCoordinates[1] = coordinateNumber;

        return gridCoordinates;
    }
}
